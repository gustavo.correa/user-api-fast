# Microserviço User-API

Microserviço responsavel pelo gerenciamento de usuários

## Desenvolvimento

```bash
make install-dev-deps
make run-dev-containers
```

## Produção

```bash
make install-prod-deps
make run
```

## Variáveis de Ambiente

`ENV`: Ambiente em que a aplicação está rodando. Opções: (dev, test, prod)

`DB_NAME`: Nome do banco de dados. (Opcional em `ENV=prod`, type=string)

`DB_USER`: User do banco de dados. (Requerido em `ENV=prod`, type=string)

`DB_PASSWORD`: Senha do banco de dados. (Requerido em `ENV=prod`, type=string)

`DB_HOST`: Host do banco de dados. (Requerido em `ENV=prod`, type=string)

`DB_PORT`: Porta do banco de dados .(Opcional em `ENV=prod`, type=int)

`REDIS_URI`: URI de acesso ao redis. Utilizar esquema redis:// ou rediss:// (Requerido em `ENV=prod`, type=string)

`CACHE_POOL_SIZE`: Quantidade de conexões ao redis que estarão na pool. (Opcional em `ENV=prod`, type=int, default=10)

`CACHE_TTL`: Tempo até expiração de um dado em cache (Opcional em `ENV=prod`, type=int, default=86400)

`ELASTIC_SERVICE_NAME`: Nome do Serviço que será usado no Elastic APM(Opcional em `ENV=prod`, type=string)

`ELASTIC_SECRET_TOKEN`: Token para acesso ao Elastic APM(Opcional em `ENV=prod`, type=string)

`SECRET_KEY`: Chave secreta para encriptar os dados do usuário

## Documentação da API

Para consultar a documentação da API, basta executar:
```bash
make run-dev
``` 
[Abrir a documentação](http://localhost:3333/docs)
