import re
from typing import TYPE_CHECKING, Generic, Optional, TypeVar

from pydantic import BaseModel, create_model

_to_camel_exp = re.compile("_([a-zA-Z])")


class DTO(BaseModel):
    class Config:
        allow_population_by_field_name = True
        allow_mutation = False
        use_enum_values = True

        @classmethod
        def alias_generator(cls, string):
            return re.sub(_to_camel_exp, lambda match: match[1].upper(), string)


class Details(DTO):
    last_id: Optional[int] = None
    total_pages: Optional[int] = None
    info: Optional[str] = None
    total_left: Optional[int] = None


if TYPE_CHECKING:  # pragma: no cover
    DTO_T = TypeVar("DTO_T", bound=DTO)

    class _EmbedArray(DTO, Generic[DTO_T]):
        data: list[DTO_T]
        details: Details


def embed_array(dto: "type[DTO_T]", mod: str) -> type["_EmbedArray[DTO_T]"]:
    return create_model(f"{dto.__qualname__}EmbedArray", __base__=DTO, __module__=mod, data=(list[dto], ...), details=(Details, Details()))  # type: ignore
