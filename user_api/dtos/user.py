import re
from datetime import datetime
from typing import Optional

from pydantic import constr, validator
from pydantic.networks import EmailStr

from .base import DTO, embed_array

fifty_len_str = constr(max_length=50, strip_whitespace=True)
phone_number_str = constr(
    max_length=20,
    strip_whitespace=True,
    regex=r"^55[1-9]{2}(?:[2-8]|9[0-9])[1-9]{3}[0-9]{4}$",
)
cpf_str = constr(max_length=14, min_length=14, strip_whitespace=True)


def cpf_digit_generator(numbers: list[int], position: int):
    result = [item * (position + 1 - idx) for idx, item in enumerate(numbers)]
    result = sum(result) % 11
    if (response := 11 - result) < 10:
        return response
    return 0


def cpf_validator(value: str):
    if not value.count(".") == 2:
        raise_for_invalid_cpf()
    if not value.count("-") == 1:
        raise_for_invalid_cpf()
    testable = value.replace(".", "").replace("-", "")
    if not testable.isnumeric():
        raise_for_invalid_cpf()
    to_validate = list(map(int, testable))

    if not (cpf_digit_generator(to_validate[:9], 9) == to_validate[9]):
        raise_for_invalid_cpf()
    if not (cpf_digit_generator(to_validate[:10], 10) == to_validate[10]):
        raise_for_invalid_cpf()
    if not (all(value.count(item) <= 3 for item in value)):
        raise_for_invalid_cpf()


def raise_for_invalid_cpf():
    raise ValueError("CPF is invalid")


def phone_cleaner(value: str):
    _, ddi, number = value.partition("55")
    return ddi + re.sub(r"\(| |\)|\+|-", "", number).removeprefix("0")


class CreateUser(DTO):
    first_name: fifty_len_str
    last_name: fifty_len_str
    cpf: cpf_str
    email: EmailStr
    phone_number: phone_number_str

    @validator("cpf")
    def validate_cpf(cls, value: str):
        cpf_validator(value)
        return value

    @validator("phone_number", pre=True)
    def cleanup_phone(cls, value: str):
        val = phone_cleaner(value)
        return val


class User(CreateUser):
    id: int
    created_at: datetime
    updated_at: Optional[datetime]


class EditUser(DTO):
    first_name: Optional[fifty_len_str] = None
    last_name: Optional[fifty_len_str] = None
    cpf: Optional[str] = None
    email: Optional[EmailStr] = None
    phone_number: Optional[phone_number_str] = None

    @validator("cpf", always=True)
    def validate_cpf(cls, value: Optional[str]):

        return value and cpf_validator(value)

    @validator("phone_number", pre=True, always=True)
    def cleanup_phone(cls, value: str):
        return value and phone_cleaner(value)


UserArray = embed_array(User, __name__)
