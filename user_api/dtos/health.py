from .base import DTO


class Health(DTO):
    status: bool
