from contextlib import asynccontextmanager, contextmanager
from dataclasses import dataclass
from functools import partial, wraps
from json import dumps
from typing import (
    TYPE_CHECKING,
    Any,
    AsyncGenerator,
    Awaitable,
    Callable,
    Optional,
    TypeVar,
    Union,
    overload,
)

import aioredis
from fastapi import FastAPI, Request
from typing_extensions import ParamSpec

from user_api.exc import UnexpectedError, api_error_handler

if TYPE_CHECKING:  # pragma: no cover
    from user_api.dtos.base import DTO_T

T = TypeVar("T")
P = ParamSpec("P")


class CacheMissException(Exception):
    pass


@contextmanager
def _cache_exception_handler():
    try:
        yield
    except aioredis.RedisError as err:
        raise CacheMissException from err


def _cache_decorator(func: Callable[P, Awaitable[T]]) -> Callable[P, Awaitable[T]]:
    @wraps(func)
    async def inner(*args: P.args, **kwargs: P.kwargs):
        with _cache_exception_handler():
            return await func(*args, **kwargs)

    return inner


@dataclass
class CacheConfig:
    conn_uri: str
    max_connections: int = 10
    ttl: int = 3600 * 24
    key: str = "user"


class CacheProvider:
    exc = CacheMissException

    def __init__(self, config: CacheConfig) -> None:
        self.config = config
        self.client = self.connect()

    def connect(self) -> aioredis.Redis:
        """Connects to cache source"""
        return aioredis.from_url(
            self.config.conn_uri,
            max_connections=self.config.max_connections,
        )

    @overload
    async def get(self, key: str, parser: None) -> str:
        ...

    @overload
    async def get(self, key: str, parser: Callable[[str], T]) -> T:
        ...

    @_cache_decorator
    async def get(
        self, key: str, parser: Optional[Callable[[str], T]] = None
    ) -> Union[T, str]:
        """Retrieve cached value from source. Raises `CacheMissException` if misses"""
        async with self.begin() as conn:
            value = await conn.get(key)
            if value is None:
                raise CacheMissException
            value_parser = parser or self.default_parser
            return value_parser(value)

    @staticmethod
    def default_parser(val: str) -> str:
        return str(val)

    @_cache_decorator
    async def set(self, key: str, val: str) -> None:
        """Set cached value with key on source."""
        async with self.begin() as conn:
            setter = partial(conn.set, name=key, value=val)
            await conn.rpush(self.config.key, key)
            if self.config.ttl <= 0:
                return await setter()
            return await setter(ex=self.config.ttl)

    @_cache_decorator
    async def revoke(self):
        """Delete cached value on source."""
        async with self.begin() as conn:
            await conn.delete(
                self.config.key, *(await conn.lrange(self.config.key, 0, -1))
            )

    @asynccontextmanager
    async def begin(self) -> AsyncGenerator[aioredis.Redis, None]:
        """Returns connection as context manager"""
        async with self.client.client() as conn:
            yield conn

    async def get_or_callback(
        self,
        key: str,
        parser: Callable[[str], "DTO_T"],
        callback: Callable[P, Awaitable["DTO_T"]],
        *args: P.args,
        **kwargs: P.kwargs,
    ) -> "DTO_T":
        """Retrieve cached value or calls `callback(*args, **kwargs)`, caches and return value"""
        try:
            response = await self.get(key, parser)
        except CacheMissException:
            response = await callback(*args, **kwargs)
            await self.set(key, response.json(by_alias=True))
        return response

    async def publish(self, event: str, data: dict[str, Any]):
        async with self.begin() as conn:
            await conn.publish("events", dumps(data | {"event": event}))

    @staticmethod
    def exception_handler(app: FastAPI):
        def _exception_handler(req: Request, _: CacheMissException):
            return api_error_handler(req, UnexpectedError())

        app.add_exception_handler(CacheMissException, _exception_handler)
