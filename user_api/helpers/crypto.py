from cryptography.fernet import Fernet

class InvalidKeyError(Exception):
    
    def __str__(self) -> str:
        return "The secret key provided is invalid, run 'poetry run create_secret_key' for a valid key"


class CryptoProvider:
    def __init__(self, key: str) -> None:
        try:
            self.fernet = Fernet(key.encode())
        except ValueError as err:
            raise InvalidKeyError from err

    @staticmethod
    def generate_key():
        return Fernet.generate_key().decode()

    def encrypt(self, secret: str):
        return self.fernet.encrypt(secret.encode()).decode()

    def decrypt(self, digest: str):
        return self.fernet.decrypt(digest.encode()).decode()
