from fastapi import APIRouter

from user_api.api import user
from user_api.dtos.health import Health

router = APIRouter()


@router.get("/health", response_model=Health)
def validate_health():
    return {"status": True}


router.include_router(user.router, prefix="/user", tags=["User"])
