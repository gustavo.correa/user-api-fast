from datetime import date
from typing import Optional
from urllib.parse import urlencode

from fastapi import APIRouter, BackgroundTasks, Body, Depends, Path, Query, Response

from user_api.database.provider import DatabaseProvider
from user_api.domain import UserUseCase
from user_api.dtos import user
from user_api.helpers.cache import CacheProvider

from .dependencies import (
    CacheDepends,
    DatabaseDepends,
    PaginatorWrapper,
    get_with_paginator,
)

router = APIRouter()


@router.post("/", response_model=user.User, status_code=201)
async def create_user(
    background_task: BackgroundTasks,
    payload: user.CreateUser,
    database_provider: DatabaseProvider = DatabaseDepends(),
    cache_provider: CacheProvider = CacheDepends(),
):
    background_task.add_task(cache_provider.revoke)
    return await UserUseCase(database_provider).create(payload)


@router.get("/id/{id}", response_model=user.User)
async def get_user_by_id(
    id: int = Path(...),
    database_provider: DatabaseProvider = DatabaseDepends(),
    cache_provider: CacheProvider = CacheDepends(),
):
    key = f"userid-{id}"
    return await cache_provider.get_or_callback(
        key,
        user.User.parse_raw,
        UserUseCase(database_provider).get_by_id,
        id=id,
    )



@router.get("/", response_model=user.UserArray)
async def list_users(
    first_name: Optional[str] = Query(None),
    last_name: Optional[str] = Query(None),
    created_at: Optional[date] = Query(None),
    updated_at: Optional[date] = Query(None),
    paginator_wrapper: PaginatorWrapper[UserUseCase] = Depends(
        get_with_paginator(UserUseCase)
    ),
    cache_provider: CacheProvider = CacheDepends(),
):
    params = urlencode(
        {
            "first_name": first_name,
            "last_name": last_name,
            "created_at": created_at,
            "updated_at": updated_at,
        }
    )
    key = f"userlistpage-{paginator_wrapper.paginator.last_pk}/{paginator_wrapper.paginator.limit}?{params}"

    return await cache_provider.get_or_callback(
        key,
        user.UserArray.parse_raw,
        paginator_wrapper.obj.list,
        paginator=paginator_wrapper.paginator,
        first_name=first_name,
        last_name=last_name,
        created_at=created_at,
        updated_at=updated_at,
    )


@router.put("/{id}", status_code=204)
async def update_user(
    background_task: BackgroundTasks,
    id: int = Path(...),
    payload: user.EditUser = Body(...),
    database_provider: DatabaseProvider = DatabaseDepends(),
    cache_provider: CacheProvider = CacheDepends(),
):
    await UserUseCase(database_provider).update(id, payload)
    background_task.add_task(cache_provider.revoke)
    return Response(status_code=204)


@router.delete("/{id}", status_code=204)
async def delete_user(
    background_task: BackgroundTasks,
    id: int = Path(...),
    database_provider: DatabaseProvider = DatabaseDepends(),
    cache_provider: CacheProvider = CacheDepends(),
):
    await UserUseCase(database_provider).delete(id)
    background_task.add_task(cache_provider.revoke)
    background_task.add_task(cache_provider.publish, "delete_user", {"user_id": id})
    return Response(status_code=204)
