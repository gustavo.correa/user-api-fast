from typing import Callable, Generic, TypeVar

from fastapi import Depends, Request
from fastapi.param_functions import Query

from user_api.database.paginator import Paginator
from user_api.database.provider import DatabaseProvider
from user_api.dtos.base import DTO
from user_api.helpers.cache import CacheProvider


def _get_database_provider(request: Request) -> DatabaseProvider:
    return request.app.state.database_provider


def DatabaseDepends():
    return Depends(_get_database_provider)


def _get_cache_provider(request: Request) -> CacheProvider:
    return request.app.state.cache_provider


def CacheDepends():
    return Depends(_get_cache_provider)


T = TypeVar("T")


class PaginatorWrapper(DTO, Generic[T]):
    """Representation of PaginatorWrapper"""

    paginator: Paginator
    obj: T

    class Config:
        arbitrary_types_allowed = True


def get_with_paginator(klass: type[T]) -> Callable[..., PaginatorWrapper[T]]:
    def _get_with_paginator(
        database_provider: DatabaseProvider = DatabaseDepends(),
        last_id: int = Query(0, ge=0),
        per_page: int = Query(20, ge=5, le=100),
    ):
        return PaginatorWrapper(
            paginator=Paginator(limit=per_page, last_pk=last_id),
            obj=klass(database_provider),
        )

    return _get_with_paginator
