from sqlalchemy import Column, DateTime, Index, String

from user_api.database.entity import Entity


class UserModel(Entity):
    """Database Representation for User"""

    __tablename__ = "user"

    first_name = Column(String(50))
    last_name = Column(String(50))
    cpf = Column(String(255), unique=True)
    email = Column(String(255))
    phone_number = Column(String(255))
    created_at = Column(DateTime)
    updated_at = Column(DateTime)

    Index("first_last_name_user_idx", first_name, last_name)
