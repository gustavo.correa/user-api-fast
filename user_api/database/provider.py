import logging
from contextlib import asynccontextmanager, contextmanager
from typing import AsyncGenerator, Generator

from sqlalchemy import create_engine, func, text
from sqlalchemy.engine import Connection
from sqlalchemy.ext.asyncio import AsyncSession, create_async_engine
from sqlalchemy.orm import Session, sessionmaker

from .utils import DatabaseConfig  # pylint: disable=relative-beyond-top-level


class DatabaseProvider:
    def __init__(self, database_config: DatabaseConfig, logger: logging.Logger) -> None:
        self.config = database_config
        self.logger = logger
        self.engine, self.sync_engine = self._create_engine()

    def _create_engine(self):
        return create_async_engine(
            self.config.get_uri(is_async=True), **self.config.pool_config
        ), create_engine(self.config.get_uri(is_async=False), **self.config.pool_config)

    def get_session(self):
        return sessionmaker(bind=self.engine, expire_on_commit=False, class_=AsyncSession)()  # type: ignore

    @asynccontextmanager
    async def begin(self) -> AsyncGenerator[AsyncSession, None]:
        async with self.get_session() as session:  # type: ignore
            async with session.begin():
                yield session

    def get_sync_session(self):
        return sessionmaker(bind=self.sync_engine, expire_on_commit=False)()  # type: ignore

    @contextmanager
    def sync(self) -> Generator[Session, None, None]:
        with self.get_sync_session() as session:  # type: ignore
            with session.begin():
                yield session

    async def last_inserted_id(self, conn: AsyncSession):
        return await conn.execute(func.last_insert_id())

    def sync_last_inserted_id(self, conn: Connection):
        return conn.execute(func.last_insert_id())

    async def healthcheck(self):
        try:
            async with self.begin() as conn:
                await conn.execute(text("SELECT 1"))
            return True
        except Exception as err:
            self.logger.exception(err)
            return False

    def sync_healthcheck(self):
        try:
            with self.sync() as conn:
                conn.execute(text("SELECT 1"))
            return True
        except Exception as err:
            self.logger.exception(err)
            return False
