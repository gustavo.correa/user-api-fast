from pathlib import Path

from user_api.core.settings import BASE_DIR

from .model_finder import ModelFinder


def get_metadata(base_dir: Path = BASE_DIR):
    from .entity import Entity

    model_finder = ModelFinder(base_dir / "database" / "models", base_dir)
    return model_finder.preload_metadata(Entity.metadata)
