from datetime import datetime
from typing import Any

from sqlalchemy import func
from sqlalchemy.exc import IntegrityError, MultipleResultsFound, NoResultFound
from sqlalchemy.future import select
from sqlalchemy.sql.expression import delete, update

from user_api import exc
from user_api.database.paginator import Paginator
from user_api.dtos.user import CreateUser, EditUser, User, UserArray

from .filters import filters
from .models.user import UserModel
from .provider import DatabaseProvider


class Repository:
    def __init__(self, database_provider: DatabaseProvider):
        self.database_provider = database_provider

    async def create(self, dto: dict, created_at: datetime):
        async with self.database_provider.begin() as session:
            user = UserModel(**dto, created_at=created_at)
            session.add(user)
            try:
                await session.flush()
            except IntegrityError as err:
                raise exc.AlreadyExists("User") from err
            else:
                return self.to_dto(user)

    @staticmethod
    def to_dto(user: UserModel):
        return {
                "id": user.id,
                "first_name": user.first_name,
                "last_name": user.last_name,
                "cpf": user.cpf,
                "email": user.email,
                "phone_number": user.phone_number,
                "created_at": user.created_at,
                "updated_at": user.updated_at,
            }

    async def get(self, field: str, val: Any):
        query = select(UserModel).where(
            filters.FieldFilter(field, val).whereclause(UserModel)
        )
        async with self.database_provider.begin() as session:
            result = await session.execute(query)
            try:
                user = result.scalar_one()
            except NoResultFound as err:
                raise exc.DoesNotExist("User") from err
            except MultipleResultsFound as err:
                raise exc.ApiError.from_message("Invalid user search") from err
            else:
                return self.to_dto(user)

    async def list(self, paginator: Paginator, *where: filters.Filter):
        query = select(UserModel).where(*(f.whereclause(UserModel) for f in where))
        async with self.database_provider.begin() as session:
            query = paginator.paginate_query(query, UserModel)
            response = await session.execute(query)
            result = list(map(self.to_dto, response.scalars().all()))
            return {
                    "data": result,
                    "details": await paginator.get_details(
                        self, paginator.get_last_pk(result), *where
                    ),
                }

    async def update(self, id: int, payload: dict, updated_at: datetime):
        await self.get("id", id)
        query = (
            update(UserModel)
            .where(UserModel.id == id)
            .values(
                **payload,
                updated_at=updated_at
            )
        )
        async with self.database_provider.begin() as session:
            try:
                await session.execute(query)
            except IntegrityError as err:
                raise exc.AlreadyExists.from_message(
                    "A duplicate value was sent, check your payload and try again"
                ) from err

    async def count(self, *where: filters.Filter):
        whereclause = list(f.whereclause(UserModel) for f in where)
        query = select(func.count(UserModel.id)).where(*whereclause)
        async with self.database_provider.begin() as conn:
            result = await conn.execute(query)
            return result.scalar_one()

    async def delete(self, id: int):
        query = delete(UserModel).where(UserModel.id == id)
        async with self.database_provider.begin() as session:
            result = await session.execute(query)
            if not result.rowcount:  # type: ignore
                raise exc.DoesNotExist("User")
