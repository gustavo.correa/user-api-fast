import importlib
import inspect
from collections import defaultdict
from pathlib import Path
from typing import Any, Generic, Optional, TypeVar

from sqlalchemy import MetaData

from .entity import Entity

T = TypeVar("T", bound=object)


class ModelFinder(Generic[T]):
    def __init__(
        self,
        path: Path,
        root: Path = None,
    ):
        self.path = path
        self.mapping = defaultdict(dict)
        self.root = root or path
        self.child_of = Entity

    def find(self):
        self.find_from_dir()

    def find_from_dir(self, path: Optional[Path] = None):
        if path is None:
            path = self.path
        if "pycache" in path.name or ".pyc" in path.name:
            return
        for item in path.iterdir():
            if item.is_dir():
                self.find_from_dir(item)
                continue
            self.find_from_file(item)

    def _save_child_of(self, name: str, import_source: str, obj: Any):
        if inspect.isclass(obj):
            if (
                issubclass(obj, self.child_of)
                and name != self.child_of.__qualname__
                and obj.__module__ == import_source
            ):
                self.mapping[name].update({import_source: obj})

    def find_from_file(self, path: Optional[Path] = None):
        if path is None:
            path = self.path
        if "__init__" in path.name:
            return
        import_source = self.get_import(path)
        mod = importlib.import_module(import_source)
        for name, obj in inspect.getmembers(mod):
            self._save_child_of(name, import_source, obj)

    def get_import(self, target: Path):
        target_str = target.as_posix()
        result = (
            target_str.replace(self.root.as_posix(), self.root.name)
            .replace("/", ".")
            .replace(".py", "")
        )
        return result

    def preload_metadata(self, metadata: MetaData):
        self.find()
        return metadata
