from typing import TYPE_CHECKING, Callable, TypeVar

from sqlalchemy import Column, Integer, MetaData
from sqlalchemy.orm.decl_api import as_declarative, declared_attr

T = TypeVar("T")


def _as_declarative(cls: type[T]) -> type[T]:
    return as_declarative()(cls)  # type: ignore


@_as_declarative
class Entity:
    if TYPE_CHECKING:  # pragma: no cover
        __name__: str
        metadata: MetaData
        __init__: Callable[..., None]

    @declared_attr
    def id(cls):
        return Column(Integer, primary_key=True, autoincrement=True)
