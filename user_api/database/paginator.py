import math
from typing import TYPE_CHECKING

from sqlalchemy.sql import Select

from user_api.database.entity import Entity

from .filters import comparison
from .filters.filters import FieldFilter, Filter

if TYPE_CHECKING:  # pragma: no cover
    from .repository import Repository


class Paginator:
    def __init__(self, limit: int = 10, last_pk: int = 0, apply: bool = True) -> None:
        self.limit = limit
        self.last_pk = last_pk
        self.apply = apply

    def paginate_query(self, query: Select, entity: type[Entity]):
        if not self.apply:
            return query
        return query.where(entity.id > self.last_pk).limit(self.limit)  # type: ignore

    async def get_details(
        self,
        repository: "Repository",
        last_pk: int,
        *where: Filter,
    ):
        count = await repository.count(*where)
        total_pages = math.ceil(count / self.limit)
        if last_pk != -1:
            total_left = await repository.count(
                *where, FieldFilter("id", last_pk, comp=comparison.Greater())
            )
        else:
            total_left = 0
            last_pk = self.last_pk
        return {
            "last_id": last_pk,
            "total_pages": total_pages,
            "total_left": total_left,
        }

    @staticmethod
    def get_last_pk(results: list):
        try:
            last_result = results[-1]
        except IndexError:
            return -1
        else:
            return last_result["id"]
