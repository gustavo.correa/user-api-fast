import abc
from abc import abstractmethod
from dataclasses import dataclass
from enum import Enum
from typing import Any, Callable, Optional, Type, Union

from sqlalchemy import Column, and_, false, or_, true
from sqlalchemy.orm.relationships import RelationshipProperty
from sqlalchemy.sql.elements import BooleanClauseList
from sqlalchemy.sql.functions import func

from user_api.database.entity import Entity

from . import comparison


class Filter(abc.ABC):
    @abc.abstractmethod
    def whereclause(self, entity: Type[Entity]):
        """Receives a SQLAlchemy instance and returns a queryable statement"""

    @abstractmethod
    def __bool__(self):
        ...


@dataclass
class FieldFilter(Filter):
    field: str
    value: Optional[Any]
    comp: comparison.Comparison = comparison.Equal()
    enum_value: bool = False
    sql_func: Optional[Callable[[Column], Any]] = None

    def __eq__(self, filter: Filter) -> bool:
        if not isinstance(filter, type(self)):
            return False
        return (self.field, self.value, self.comp) == (
            filter.field,
            filter.value,
            filter.comp,
        )

    def __post_init__(self):
        if isinstance(self.value, bool) and not isinstance(self.comp, comparison.Null):
            self.value = true() if self.value else false()
        if isinstance(self.value, Enum):
            if self.enum_value:
                self.value = self.value and self.value.value
            else:
                self.value = self.value and self.value.name

    def whereclause(self, entity: Type[Entity]):
        return self.attr(entity)  # type: ignore

    def attr(self, entity: Type[Entity]):
        attr = self._attr(entity, self.field)
        if not self:
            return True
        if self.sql_func:
            attr = self.sql_func(attr)  # type: ignore
        return self.comp.compare(attr, self.value)

    @staticmethod
    def _attr(entity: Type[Entity], field: str) -> Union[Column, RelationshipProperty]:
        result = getattr(entity, field, None)
        if result is None:
            raise NotImplementedError
        return result

    def __bool__(self):
        return self.value is not None


@dataclass(init=False)
class FilterJoins(Filter):
    operator: Type[BooleanClauseList]
    filters: tuple[Filter, ...]

    def __init__(self, *filters: Filter) -> None:
        self.filters = filters

    def whereclause(self, entity: Type[Entity]):
        return self.operator(*(f.whereclause(entity) for f in self.filters))

    def __bool__(self):
        return True


class OrFilter(FilterJoins):
    @property
    def operator(self):
        return or_


class AndFilter(FilterJoins):
    @property
    def operator(self):
        return and_


def date_func(col: Column):
    return func.date(col)
