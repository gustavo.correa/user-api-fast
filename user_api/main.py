import logging

import secure
from fastapi import APIRouter, FastAPI
from fastapi.middleware.cors import CORSMiddleware
from starlette.middleware.base import BaseHTTPMiddleware

from user_api.helpers.cache import CacheConfig, CacheProvider

from .api import root
from .core import settings
from .database.provider import DatabaseProvider
from .database.utils import DatabaseConfig, Driver, MysqlDriver, PostgresDriver
from .exc import set_api_error_handler


def create_startup_handler(
    _app: FastAPI,
    database_config: DatabaseConfig,
    cache_config: CacheConfig,
    logger: logging.Logger,
):
    def _startup():
        _app.state.database_provider = DatabaseProvider(database_config, logger)
        _app.state.cache_provider = CacheProvider(cache_config)

    return _startup


def setup_security_middleware(_app: FastAPI):
    server = secure.Server().set("Secure")
    xss = secure.XXSSProtection()

    hsts = (
        secure.StrictTransportSecurity().include_subdomains().preload().max_age(2592000)
    )

    referrer = secure.ReferrerPolicy().no_referrer()

    cache_value = secure.CacheControl().must_revalidate()

    secure_headers = secure.Secure(
        server=server,
        xxp=xss,
        hsts=hsts,
        referrer=referrer,
        cache=cache_value,
    )

    async def set_secure_headers(request, call_next):
        response = await call_next(request)
        secure_headers.framework.fastapi(response)
        return response

    _app.add_middleware(BaseHTTPMiddleware, dispatch=set_secure_headers)


def get_application(
    *,
    title: str,
    prefix: str = "",
    logger: logging.Logger,
    router: APIRouter,
    database_config: DatabaseConfig,
    cache_config: CacheConfig,
):
    _app = FastAPI(
        title=title,
        openapi_url=f"/openapi.json",
        docs_url=f"/docs",
        redoc_url=f"/redoc",
        root_path=f"/{prefix}",
    )
    _app.include_router(router)

    _app.add_event_handler(
        "startup", create_startup_handler(_app, database_config, cache_config, logger)
    )
    set_api_error_handler(_app)
    CacheProvider.exception_handler(_app)
    CORSMiddleware(_app, allow_origins=["*"], allow_methods=["*"], allow_headers=["*"])
    setup_security_middleware(_app)
    settings.apm_config.setup(_app)
    return _app


def get_database_config():
    driver_klass = MysqlDriver if settings.DB_TYPE == Driver.MYSQL else PostgresDriver
    return DatabaseConfig(
        driver=driver_klass(),
        host=settings.DB_HOST,
        user=settings.DB_USER,
        passwd=settings.DB_PASSWORD,
        port=settings.DB_PORT,
        name=settings.DB_NAME,
    )


def get_cache_config():
    return CacheConfig(
        conn_uri=settings.CACHE_URI, max_connections=settings.CACHE_POOL_SIZE
    )


app = get_application(
    title="User API",
    logger=settings.logger,
    database_config=get_database_config(),
    cache_config=get_cache_config(),
    router=root.router,
    prefix="user",
)


def main():
    import uvicorn

    uvicorn.run(
        "user_api.main:app",
        port=3333,
        reload=True,
        lifespan="on",
        log_level="debug",
    )


def healthcheck():
    from time import sleep

    database_provider = DatabaseProvider(get_database_config(), settings.logger)
    settings.logger.setLevel(logging.FATAL)
    while not database_provider.sync_healthcheck():
        # Waiting for database to be ready
        sleep(5)
    # Database is ready to connection
