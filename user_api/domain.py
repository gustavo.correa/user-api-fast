from datetime import date, datetime
from typing import Optional

from user_api.database.filters import filters
from user_api.database.filters.comparison import InsensitiveLike
from user_api.database.paginator import Paginator
from user_api.dtos.user import CreateUser, EditUser, User, UserArray
from user_api.core import settings
from user_api.database.provider import DatabaseProvider
from user_api.database.repository import Repository


class UserUseCase:
    def __init__(self, database_provider: DatabaseProvider):
        self.database_provider = database_provider
        self.user_repository = Repository(database_provider)
        self.secret_fields = {"email", "cpf", "phone_number"}

    def encrypt_user(self, user: CreateUser):
        return user.dict(exclude=self.secret_fields) | {
            key: settings.CRYPTO.encrypt(getattr(user, key))
            for key in self.secret_fields
        }

    def decrypt_user(self, user: dict):
        for key in self.secret_fields:
            user[key] = settings.CRYPTO.decrypt(user.pop(key))
        return User.parse_obj(user)

    async def create(self, user: CreateUser):
        return self.decrypt_user(
            await self.user_repository.create(
                self.encrypt_user(user), datetime.utcnow()
            )
        )

    async def get_by_id(self, id: int):
        return self.decrypt_user(await self.user_repository.get("id", id))


    async def list(
        self,
        paginator: Paginator,
        *,
        first_name: Optional[str],
        last_name: Optional[str],
        created_at: Optional[date],
        updated_at: Optional[date],
    ):
        result = await self.user_repository.list(
            paginator,
            filters.FieldFilter("first_name", first_name, comp=InsensitiveLike()),
            filters.FieldFilter("last_name", last_name, comp=InsensitiveLike()),
            filters.FieldFilter("created_at", created_at, sql_func=filters.date_func),
            filters.FieldFilter("updated_at", updated_at, sql_func=filters.date_func),
        )
        result["data"] = list(map(self.decrypt_user, result["data"]))
        return UserArray.parse_obj(result)

    def validate_edit(self, payload: EditUser):
        result = payload.dict(exclude_unset=True, exclude_none=True)
        for item in self.secret_fields:
            if item in result:
                result[item] = settings.CRYPTO.encrypt(result.pop(item))
        return result

    async def update(self, id: int, payload: EditUser):
        return await self.user_repository.update(
            id, self.validate_edit(payload), datetime.utcnow()
        )

    async def delete(self, id: int):
        return await self.user_repository.delete(id)
