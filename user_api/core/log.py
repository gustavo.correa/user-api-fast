import logging

from elasticapm.contrib.starlette import ElasticAPM, make_apm_client
from fastapi import FastAPI

from user_api.helpers.environment import Env


def set_logger(env: Env):
    logger = logging.getLogger("uvicorn.error" if env != Env.PROD else "gunicorn.error")
    logger.setLevel(logging.INFO)
    return logger


class APMConfigField:
    def __init__(self, name: str) -> None:
        self.name = name
        self.value = None

    def set(self, value: str):
        self.value = value


class APMConfig:
    service_name: APMConfigField = APMConfigField("SERVICE_NAME")
    secret_token: APMConfigField = APMConfigField("SECRET_TOKEN")

    def set(self, key: APMConfigField):
        def inner(value: str) -> str:
            key.set(value)
            return value

        return inner

    def is_valid(self):
        return bool(self.service_name.value and self.secret_token.value)

    def setup(self, app: FastAPI):
        client = make_apm_client(
            {
                item.name: item.value
                for item in {self.service_name, self.secret_token}
                if item.value
            }
        )
        app.add_middleware(ElasticAPM, client=client)
