from pathlib import Path
from urllib.parse import quote
from user_api.helpers.crypto import CryptoProvider

from dotenv import load_dotenv

from user_api.database.utils import Driver
from user_api.helpers.environment import Env, Environment

from .log import APMConfig, set_logger

BASE_DIR = Path(__file__).resolve().parent.parent
if (env_file := (BASE_DIR.parent / ".env")).exists():
    load_dotenv(env_file)

environment = Environment()
ENV = environment.get("ENV", dev="dev", parser=Env)
required_env = environment.required_if(ENV == Env.PROD)
logger = set_logger(ENV)
apm_config = APMConfig()
environment.set_logger(logger)

# ===> Database ENV Vars <===
DB_TYPE = environment.get("DB_TYPE", dev="mysql", parser=Driver)
DB_NAME = environment.get("DB_NAME", dev="db-name", parser=quote)
DB_USER = required_env("DB_USER", dev="db-user", parser=quote)
DB_PASSWORD = required_env("DB_PASSWORD", dev="db-pass", parser=quote)
DB_HOST = required_env("DB_HOST", dev="localhost", parser=quote)
DB_PORT = environment.get("DB_PORT", dev="3306", parser=int, warn=False)

# ===> Cache ENV Vars <===
CACHE_URI = environment.get("REDIS_URI", dev="cache-uri", parser=str)
CACHE_POOL_SIZE = environment.get("CACHE_POOL_SIZE", dev="10", parser=int, warn=False)
DAY = 86400
CACHE_TTL = environment.get("CACHE_TTL", dev=str(DAY), parser=int)

# ===> Elastic APM Config <===
ELASTIC_SERVICE_NAME = environment.get(
    "ELASTIC_SERVICE_NAME", dev="", parser=apm_config.set(apm_config.service_name)
)
ELASTIC_SECRET_TOKEN = environment.get(
    "ELASTIC_SECRET_TOKEN", dev="", parser=apm_config.set(apm_config.secret_token)
)

# ===> Crypto Config <===
CRYPTO = required_env("SECRET_KEY", dev="", parser=CryptoProvider)
