help:
	@echo "  clean                       clean files"
	@echo "  install-dev-deps            install with dev dependencies"
	@echo "  install-prod-deps            install without dev dependencies"
	@echo "  run-dev-containers          run docker containers for developement"
	@echo "  test                        run the testsuite"
	@echo "  format                      format the python code"
	@echo "  check-vul                   check vulnerabilities"
	@echo "  run-local                   run the server in localhost with debug and autoreload (developpement mode, port=3333)"
	@echo "  run                         run the server in localhost (production mode, port=8000)"

.PHONY: clean
clean:
	@echo "--> Cleaning pyc files"
	find . -name "*.pyc" -delete

.PHONY: install-dev-deps
install-dev-deps:
	poetry install

.PHONY: install-prod-deps
install-prod-deps:
	poetry install --no-dev

.PHONY: run-dev-containers
run-dev-containers:
	@echo "--> Running development containers"
	docker-compose --env-file=docker.env up --build

.PHONY: test
test:
	@echo "--> Running unittest"
	poetry run pytest --verbose -p no:warning --cov=user_api --cov-report=html --cov-report=xml:.artifacts/coverage.xml --junit-xml=.artifacts/tests.xml --cov-config=.coveragerc 


.PHONY: format
format:
	@echo "--> Format the python code"
	poetry run autoflake --remove-all-unused-imports --remove-unused-variables  --recursive --in-place user_api/ tests/
	poetry run fix

.PHONY: check-vul
check-vul:
	@echo "--> Check vulnerabilities"
	poetry run bandit -r user_api/ -c bandit.yaml


.PHONY: run-dev
run-dev:
	@echo "--> Running dev server"
	poetry run start

.PHONY: migrate
migrate:
	@echo "--> Running migrations"
	poetry run alembic upgrade head

.PHONY: run
run:
	@echo "--> Running server"
	poetry run gunicorn \
    -k uvicorn.workers.UvicornWorker \
    --bind 0.0.0.0:8000 \
    --name user-api \
    --log-level=info \
    user_api.main:app
