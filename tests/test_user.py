from typing import cast

import pytest
from faker import Faker
from fastapi.testclient import TestClient
from pydantic import ValidationError

from user_api.dtos import user
from user_api.exc import AlreadyExists, DoesNotExist


def _create_user(faker_instance: Faker):
    while True:
        try:
            create_user = user.CreateUser(
                first_name=faker_instance.first_name(),
                last_name=faker_instance.last_name(),
                cpf=faker_instance.cpf(),
                email=faker_instance.email(),
                phone_number=faker_instance.cellphone_number(),
            )
        except ValidationError:
            pass
        else:
            return create_user


@pytest.fixture
def create_user(faker_instance: Faker):
    return _create_user(faker_instance)


def test_create_user(test_client: TestClient, create_user: user.CreateUser):
    response = test_client.post("/user/", create_user.json(by_alias=True))
    assert response.status_code == 201
    user.User.parse_obj(response.json())



def test_get_user_by_id(test_client: TestClient, create_user: user.CreateUser):
    response = test_client.post("/user/", create_user.json(by_alias=True))
    received_user = user.User.parse_obj(response.json())
    response = test_client.get(f"/user/id/{received_user.id}")
    assert response.status_code == 200
    assert received_user == user.User.parse_obj(response.json())


def test_get_non_existent_user_fails(test_client: TestClient, faker_instance: Faker):
    id_response = test_client.get(f"/user/id/{0}")
    assert id_response.status_code == 404
    assert id_response.json() == {"message": DoesNotExist("User").get_message()}


def create_many_users(test_client: TestClient, faker_instance: Faker, n: int = 5):
    for _ in range(n):
        test_client.post("/user/", _create_user(faker_instance).json(by_alias=True))


def validate_last_page(
    test_client: TestClient, payload: user.UserArray, page_size: int
):
    assert payload.details.total_pages == 1
    response = test_client.get(
        "/user/",
        params={"last_id": payload.details.last_id, "per_page": page_size},
    )
    payload = user.UserArray.parse_obj(response.json())
    assert not payload.data
    assert not payload.details.total_left


def validate_pagination(test_client: TestClient, page_size: int, last_id: int):
    response = test_client.get(
        "/user/",
        params={"last_id": last_id, "per_page": page_size},
    )
    payload = user.UserArray.parse_obj(response.json())
    assert not payload.details.total_left or (
        payload.details.last_id != last_id
    ), "should have different last ids if there is still items left"
    return payload.details.last_id


def test_list_user(test_client: TestClient, faker_instance: Faker):
    create_many_users(test_client, faker_instance, 10)
    PAGE_SIZE = 5
    payload = user.UserArray.parse_obj(
        test_client.get("/user/", params={"per_page": PAGE_SIZE}).json()
    )
    last_id = payload.details.last_id
    assert len(payload.data) == PAGE_SIZE
    assert last_id == payload.data[-1].id
    if not payload.details.total_left:
        validate_last_page(test_client, payload, PAGE_SIZE)
    else:
        for _ in range(payload.details.total_pages):  # type: ignore
            last_id = validate_pagination(test_client, PAGE_SIZE, cast(int, last_id))


def test_edit_user(
    test_client: TestClient, create_user: user.CreateUser, faker_instance: Faker
):
    payload = user.User.parse_obj(
        test_client.post("/user/", create_user.json(by_alias=True)).json()
    )
    response = test_client.put(
        f"/user/{payload.id}",
        user.EditUser(
            first_name=faker_instance.first_name(), last_name=faker_instance.last_name()
        ).json(by_alias=True),
    )
    assert response.status_code == 204
    edited_payload = user.User.parse_obj(
        test_client.get(f"/user/id/{payload.id}").json()
    )
    assert (edited_payload.first_name, edited_payload.last_name,) != (
        payload.first_name,
        payload.last_name,
    )


def test_delete_user(test_client: TestClient, create_user: user.CreateUser):
    payload = user.User.parse_obj(
        test_client.post("/user/", create_user.json(by_alias=True)).json()
    )
    response = test_client.delete(f"/user/{payload.id}")
    assert response.status_code == 204
    response = test_client.get(f"/user/id/{payload.id}")
    assert response.status_code == 404
    assert response.json() == {"message": DoesNotExist("User").get_message()}


def test_empty_pagination(test_client: TestClient):
    delete_all_users(test_client)
    response = test_client.get("/user/", params={"per_page": 5})
    payload = user.UserArray.parse_obj(response.json())
    assert response.status_code == 200
    assert not payload.data
    assert payload.details.last_id == 0
    assert payload.details.total_left == 0
    assert payload.details.total_pages == 0


def delete_all_users(test_client: TestClient):
    PAGE_SIZE = 5
    payload = user.UserArray.parse_obj(
        test_client.get("/user/", params={"per_page": PAGE_SIZE}).json()
    )
    last_id = payload.details.last_id
    for _ in range(payload.details.total_pages):  # type: ignore
        for item in payload.data:
            test_client.delete(f"/user/{item.id}")
        response = test_client.get(
            "/user/",
            params={"last_id": last_id, "per_page": PAGE_SIZE},
        )
        payload = user.UserArray.parse_obj(response.json())
        last_id = payload.details.last_id
