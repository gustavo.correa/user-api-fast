from fakeredis import aioredis

from user_api.helpers.cache import CacheProvider


class MockProvider(CacheProvider):
    """Mock cache provider for writing unit tests."""

    def connect(self):
        return aioredis.FakeRedis()
