import logging
import os
from pathlib import Path

import pytest

os.environ["ENV"] = "test"

import faker
from fastapi import FastAPI
from fastapi.middleware.cors import CORSMiddleware
from fastapi.testclient import TestClient

from user_api.api import root
from user_api.helpers.cache import CacheConfig
from user_api.core import settings
from user_api.database.metadata import get_metadata
from user_api.database.provider import DatabaseConfig, DatabaseProvider
from user_api.database.utils import SqliteDriver
from user_api.exc import set_api_error_handler
from user_api.main import setup_security_middleware

from .mock import MockProvider

ROOT_DIR = Path(__file__).resolve().parent.parent
db_root_dir = ROOT_DIR / "tmp" / "db.sqlite"
if not (tmp := db_root_dir.parent).exists():
    tmp.mkdir()


faker.Faker.seed(os.urandom(32))


@pytest.fixture(scope="session")
def faker_instance():
    return faker.Faker(["en-US", "en_US", "pt_BR", "pt-BR"])


def create_startup_handler(
    _app: FastAPI,
    database_config: DatabaseConfig,
    cache_config: CacheConfig,
    logger: logging.Logger,
):
    def _startup():
        _app.state.database_provider = DatabaseProvider(database_config, logger)
        metadata = get_metadata()
        metadata.create_all(_app.state.database_provider.sync_engine)
        _app.state.cache_provider = MockProvider(cache_config)

    return _startup


def create_shutdown_handler(_: FastAPI):
    def _shutdown():
        db_root_dir.unlink(missing_ok=True)

    return _shutdown


def get_test_client():
    _app = FastAPI(
        title="Test Application",
        openapi_url=f"/openapi.json",
        docs_url=f"/docs",
        redoc_url=f"/redoc",
        root_path="/user",
    )
    _app.include_router(root.router)

    database_config = DatabaseConfig(SqliteDriver(), host=str(db_root_dir))
    cache_config = CacheConfig(":memory:")
    logger = settings.logger
    _app.add_event_handler(
        "startup", create_startup_handler(_app, database_config, cache_config, logger)
    )
    _app.add_event_handler("shutdown", create_shutdown_handler(_app))
    set_api_error_handler(_app)
    MockProvider.exception_handler(_app)
    CORSMiddleware(_app, allow_origins=["*"], allow_methods=["*"], allow_headers=["*"])
    setup_security_middleware(_app)
    return TestClient(_app, root_path="/user")


@pytest.fixture(scope="session")
def test_client():
    with get_test_client() as client:
        yield client
