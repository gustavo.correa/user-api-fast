import asyncio
import logging
from user_api.helpers.crypto import CryptoProvider, InvalidKeyError

import pytest
from faker import Faker
from fastapi.testclient import TestClient

from tests.conftest import ROOT_DIR
from user_api.core import settings
from user_api.database.provider import DatabaseProvider
from user_api.database.utils import DatabaseConfig, MysqlDriver, SqliteDriver
from user_api.dtos.user import cpf_validator

db_root_dir = ROOT_DIR / "tmp" / "db.sqlite"


def test_cpf_validator(faker_instance: Faker):
    valid_cpf = faker_instance.cpf()
    invalid_cpfs = [
        "123.456.789-44",
        "123 455 678 91",
        "11111111111111111",
        "111.111.111-11",
        "111.111111-11",
        "111.111.111/11",
        "abc.def.ghi-jk",
    ]
    cpf_validator(valid_cpf)
    with pytest.raises(ValueError, match="CPF is invalid"):
        for invalid_cpf in invalid_cpfs:
            cpf_validator(invalid_cpf)


def test_headers(test_client: TestClient):
    response = test_client.get("/user/")
    assert all(
        item in response.headers
        for item in [
            "X-XSS-Protection",
            "Strict-Transport-Security",
            "Referrer-Policy",
            "Cache-Control",
        ]
    )


def test_sync_db_provider(caplog):
    db_config = DatabaseConfig(SqliteDriver(), host=str(db_root_dir))
    provider = DatabaseProvider(db_config, settings.logger)
    caplog.set_level(logging.INFO)
    assert provider.sync_healthcheck()
    assert not caplog.records


def test_sync_db_provider_fails_correctly(caplog):
    db_config = DatabaseConfig(MysqlDriver(), host=str(db_root_dir))
    provider = DatabaseProvider(db_config, settings.logger)
    caplog.set_level(logging.ERROR)
    assert not provider.sync_healthcheck()
    assert caplog.records


def test_aio_db_provider(caplog):
    db_config = DatabaseConfig(SqliteDriver(), host=str(db_root_dir))
    provider = DatabaseProvider(db_config, settings.logger)
    caplog.set_level(logging.INFO)
    assert asyncio.get_event_loop().run_until_complete(provider.healthcheck())
    assert not caplog.records


def test_aio_db_provider_fails_correctly(caplog):
    db_config = DatabaseConfig(MysqlDriver(), host=str(db_root_dir))
    provider = DatabaseProvider(db_config, settings.logger)
    caplog.set_level(logging.ERROR)
    assert not asyncio.get_event_loop().run_until_complete(provider.healthcheck())
    assert caplog.records


def test_crypto_provider(faker_instance: Faker):
    key = CryptoProvider.generate_key()
    crypto_provider = CryptoProvider(key)
    secret = faker_instance.sentence()
    digest = crypto_provider.encrypt(secret)
    assert digest != secret
    assert crypto_provider.decrypt(digest) == secret


def test_crypto_provider_fails_correctly():
    with pytest.raises(InvalidKeyError) as exc_info:
        CryptoProvider("")
    assert str(exc_info.value) == str(InvalidKeyError())
