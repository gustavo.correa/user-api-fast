FROM python:3.9-slim

RUN python3.9 -m pip install -U pip poetry
COPY pyproject.toml /var/www/app/
COPY poetry.lock /var/www/app/

WORKDIR /var/www/app

RUN poetry install --no-dev

COPY . /var/www/app

EXPOSE 8000

ENTRYPOINT poetry run health && poetry run alembic upgrade head && poetry run gunicorn \
    --workers 4 \
    -k uvicorn.workers.UvicornWorker \
    --bind 0.0.0.0:8000 \
    --access-logfile - \
    --name user-api \
    --log-level=info \
    user_api.main:app

