"""changed cpf email and phone number length

Revision ID: 099a06302a15
Revises: 0b6486e4d725
Create Date: 2021-08-26 07:02:31.698059

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '099a06302a15'
down_revision = '0b6486e4d725'
branch_labels = None
depends_on = None


def upgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column("user", "cpf", type_=sa.String(255))
    op.alter_column("user", "phone_number", type_=sa.String(255))
    # ### end Alembic commands ###


def downgrade():
    # ### commands auto generated by Alembic - please adjust! ###
    op.alter_column("user", "cpf", type_=sa.String(20))
    op.alter_column("user", "phone_number", type_=sa.String(20))
    # ### end Alembic commands ###
